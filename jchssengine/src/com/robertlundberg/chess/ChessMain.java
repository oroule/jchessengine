package com.robertlundberg.chess;

import java.util.List;
import java.util.Random;
import java.util.logging.*;

public class ChessMain {
        
        public static void main(String args[])
        {		       	
                Board B = new Board();
                List GeneratedMoves;
                Random generator = new Random();
                B.PrintBoardWPieceListPositions();      
                System.out.println("");         
                B.PrintBoardWPieceValues();
                
                MoveGenerator MG = new MoveGenerator(B);
                GeneratedMoves = MG.GenerateMoves();
                System.out.println("");
                for(int i=0;i < GeneratedMoves.size();i++)
                {
                        System.out.printf("(%d)",(Integer)GeneratedMoves.get(i));
                }
                System.out.println("");
                for(int i=0;i < GeneratedMoves.size();i++)
                {
                        System.out.printf("(%s-%s)",Board.SquareNameLookUp[Move.GetFromSquare((Integer)GeneratedMoves.get(i))].toString(),Board.SquareNameLookUp[Move.GetToSquare((Integer)GeneratedMoves.get(i))]);
                }               
                //System.out.printf("\n%d",B.MakeMove((Integer)GeneratedMoves.get(generator.nextInt(GeneratedMoves.size()))));          
                
                System.out.println();
                
                System.out.println(Diagnostics.minMax(MG,B, 2));
                //B.MakeMove((Integer)GeneratedMoves.get(1));
              
                //B.UnMakeMove();
                
                //B.PrintBoardWPieceListPositions();
                
                
                
        }
        
}
