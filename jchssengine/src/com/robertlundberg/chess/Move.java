package com.robertlundberg.chess;

/**
 * class Move
 * 
 * Represents static methods for encoding and decoding moves into a single integer.
 *
 * 
 * @author Robert Lundberg
 **/
public class Move {
        private static final int toShift = 7;   
        private static final int indexShift = 14;
        private static final int squareMask = 127;      
        private static final int indexMask = 15;
        
        public static  int EncodeMove(int from,int to,int index)
        {
                int move = (from) | (to << toShift) | (index << indexShift);
                return move;
        }
        public static int GetFromSquare(int move)
        {               
                return (move & squareMask);
        }
        public static int GetToSquare(int move)
        {
                return ((move >> toShift) & squareMask);
        }
        public static int GetPieceListIndex(int move)
        {
                return ((move >> indexShift) & indexMask);
        }
        
}
