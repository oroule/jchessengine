package com.robertlundberg.chess;

/**
 * class Piece
 *
 * Represents the chess pieces, acts as the base class for the different subclasses like pawn,rook etc.
 *
 * @author Robert Lundberg
 */
public class Piece
{      
        int _position;
        int _pieceValue;
        int[] _pieceDelta;
        int [] _attackDelta;
        PieceType _type;
       
        public Piece(PieceType t, int p)
        {
                this._type = t;
                this._position = p;    
        }
        static class Knight extends Piece {    
               
                public Knight(PieceType t,int p, int pv)
                {
                        super(t,p);
                        this._pieceValue = pv;
                        _pieceDelta = new int[]{14,31,33,18,-14,-31,-33,-18};                  
                }
        }
        static class Bishop extends Piece {            
               
                public Bishop(PieceType t,int p, int pv)
                {
                        super(t,p);
                        this._pieceValue = pv;  
                        _pieceDelta = new int[]{15,-15,17,-17};                
                }
        }      
        static class Rook extends Piece {
               
               
                public Rook(PieceType t,int p, int pv)
                {
                        super(t,p);
                        this._pieceValue = pv;
                        _pieceDelta = new int[]{-1,16,1,-16};                  
                }
        }
        static class King extends Piece {
               
               
                public King(PieceType t,int p, int pv)
                {
                        super(t,p);
                        this._pieceValue = pv;
                        _pieceDelta = new int[]{-1,15,16,17,1,-15,-16,-17};                    
                }
        }
        static class Queen extends Piece {
               
               
                public Queen(PieceType t,int p, int pv)
                {
                        super(t,p);
                        this._pieceValue = pv;
                        _pieceDelta = new int[]{-1,15,16,17,1,-15,-16,-17};                    
                }
        }
        static class WhitePawn extends Piece
        {              
                public WhitePawn(PieceType t,int p, int pv)
                {
                        super(t,p);
                        this._pieceValue = pv;
                        _pieceDelta = new int[]{16,32};
                        _attackDelta = new int[]{15,17};                        
                }              
        }
        static class BlackPawn extends Piece
        {              
                public BlackPawn(PieceType t,int p, int pv)
                {
                        super(t,p);
                        this._pieceValue = pv;
                        _pieceDelta = new int[]{-16,-32};
                        _attackDelta = new int[]{-15,-17};                      
                }              
        }      
       
        //enums
        public static enum PieceValues {
                Pawn(100), Knight(300), Bishop(305), Rook(500), Queen(900), King(2000);

                  private int value;    

                  private PieceValues(int value)
                  {
                    this.value = value;
                  }

                  public int getValue()
                  {
                    return value;
                  }
                }
        public static enum PieceType
        {
                WhitePawn(1), WhiteKnight(2),
                WhiteBishop(5), WhiteRook(6),
                WhiteQueen(7), WhiteKing(3),
                BlackPawn(-1),BlackKnight(-2),
                BlackBishop(-5), BlackRook(-6),
                BlackQueen(-7), BlackKing(-3);          

                private int value;    

                private PieceType(int value)
                {
                        this.value = value;
                }

                public int getValue()
                {
                        return value;
                }
        }

       
}

