package com.robertlundberg.chess;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

//TODO:Get settings from configuration file.
public class Logging {	
	
	public static void SetUpLogging(Logger L, String fileName)
	{
		try {
    	    // Create a file handler
    	    boolean append = false;
    	    FileHandler fileHandler = new FileHandler(fileName, append);
    	    Formatter f = new SimpleFormatter();
    	    fileHandler.setFormatter(f);
    	    // Add to the desired logger        	    
    	    L.addHandler(fileHandler);
    	} catch (IOException e) {
    		//TODO:Handle this exception, just re throw? 
    	}        		        		
    		L.setUseParentHandlers(false);
    		//Setting Desired log level.
    		L.setLevel(Level.INFO);	
	}
}
