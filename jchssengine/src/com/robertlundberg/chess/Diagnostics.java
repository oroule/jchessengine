package com.robertlundberg.chess;

import java.util.List;

public class Diagnostics {

        
        protected static long minMax(MoveGenerator MG, Board board, int depth)
        {  
                long nodes = 0;  
                if(depth == 0) return 1;  
                List<Integer> moves = MG.GenerateMoves();    
                for(int i = 0; i < moves.size(); i++)  
                {   
                        board.MakeMove((Integer)moves.get(i));   
                        nodes += minMax(MG, board, depth-1);    
                        board.UnMakeMove();  
                        }   
                return nodes;
        }
        
        
}
